﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class ListDirector : Form
    {
        public ListDirector()
        {
            InitializeComponent();
        }

        private async void ListDirector_Load(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                dataGridView1.AutoGenerateColumns = false;
                bindingSource1.DataSource = await entities.directors
   .Select(
      directors =>
         new
         {
             Dir_id = directors.dir_id,
             Dir_fname = directors.dir_fname,
             Dir_lname = directors.dir_lname
         }
   ).ToListAsync();
                bindingNavigator1.BindingSource = bindingSource1;
                dataGridView1.DataSource = bindingSource1;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                director director = new director()
                {
                    dir_fname = txtFirstName.Text,
                    dir_lname = txtLastName.Text              
                };
                entities.directors.Add(director);
                entities.SaveChanges();
                bindingSource1.DataSource = await entities.directors
   .Select(
      directors =>
         new
         {
             Dir_id = directors.dir_id,
             Dir_fname = directors.dir_fname,
             Dir_lname = directors.dir_lname
         }
   ).ToListAsync();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtFirstName.Clear();
            txtLastName.Clear();
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                var directorDelete = new director
                {
                    dir_id = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column3"].Value)
                };
                entities.Entry(directorDelete).State = EntityState.Deleted;
                entities.SaveChanges();
                bindingSource1.RemoveCurrent();
            }
        }

        private async void toolStripButton1_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            using (var entities = new FinalExamEntities())
            {
                bindingSource1.DataSource = await entities.directors
  .Select(
     directors =>
        new
        {
            Dir_id = directors.dir_id,
            Dir_fname = directors.dir_fname,
            Dir_lname = directors.dir_lname
        }
        )
  .ToListAsync();
                bindingNavigator1.BindingSource = bindingSource1;
                txtFirstName.DataBindings.Add("Text", bindingSource1, "dir_fname", true, DataSourceUpdateMode.OnPropertyChanged);
                txtLastName.DataBindings.Add("Text", bindingSource1, "dir_lname", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }
    }
}
