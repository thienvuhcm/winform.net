﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class ListGenre : Form
    {
        public ListGenre()
        {
            InitializeComponent();
        }

        private async void ListGenre_Load(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                bindingSource1.DataSource = await entities.genres
   .Select(
      genres =>
         new
         {
             Gen_id = genres.gen_id,
             Gen_title = genres.gen_title
         }
   ).ToListAsync();
                bindingNavigator1.BindingSource = bindingSource1;
                dataGridView1.DataSource = bindingSource1;
            }
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                genre genre = new genre()
                {
                    gen_title = txtGenre.Text,
                    
                };
                entities.genres.Add(genre);
                entities.SaveChanges();
                bindingSource1.DataSource = await entities.genres
  .Select(
     genres =>
        new
        {
            Gen_id = genres.gen_id,
            Gen_title = genres.gen_title
        }
  ).ToListAsync();
            }
            groupBox1.Visible = false;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtGenre.Clear();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void toolStripButton1_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            using (var entities = new FinalExamEntities())
            {
                bindingSource1.DataSource = await entities.genres
  .Select(
     genres =>
        new
        {
            Gen_id = genres.gen_id,
            Gen_title = genres.gen_title,
        }
        )
  .ToListAsync();
                bindingNavigator1.BindingSource = bindingSource1;
                txtGenre.DataBindings.Add("Text", bindingSource1, "gen_title", true, DataSourceUpdateMode.OnPropertyChanged);
                
            }
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                var genreDelete = new genre
                {
                    gen_id = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column1"].Value)
                };
                entities.Entry(genreDelete).State = EntityState.Deleted;
                entities.SaveChanges();
                bindingSource1.RemoveCurrent();
            }
        }
    }
}
