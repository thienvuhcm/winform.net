﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AddFilmForm addFilmForm = new AddFilmForm();
            addFilmForm.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ListFilmForm listFilmForm = new ListFilmForm();
            listFilmForm.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ListDirector listDirector = new ListDirector();
            listDirector.Show();
        }

        private void listGenresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListGenre listGenre = new ListGenre();
            listGenre.Show();
        }

        private void listActorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListActor listActor = new ListActor();
            listActor.Show();
        }
    }
}
