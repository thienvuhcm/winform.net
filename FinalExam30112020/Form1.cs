﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class loginForm : Form
    {
        public loginForm()
        {
            InitializeComponent();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                var account = (entities.reviewers
   .Where(
      reviewers =>
            (((reviewers.rev_username.Equals(txtUsername.Text)) && (reviewers.rev_password.Equals(txtPassword.Text)))))
   .Select(
      reviewers =>
         new
         {
             Rev_username = reviewers.rev_username,
             Rev_password = reviewers.rev_password,
             Rev_role = reviewers.rev_role
         }
   ));
                //var account = entities.reviewers.FirstOrDefault(u => u.rev_username == txtUsername.Text);
                if (account != null)
                {
                    //if(account.rev_password == txtPassword.Text)
                    //{
                    MainForm mainForm = new MainForm();
                    mainForm.Show();
                    //}
                    //else
                    //{
                    //    MessageBox.Show("Failed");
                    //}
                }

            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
