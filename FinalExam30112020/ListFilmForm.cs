﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class ListFilmForm : Form
    {
        public ListFilmForm()
        {
            InitializeComponent();
        }

        private async void ListFilmForm_Load(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                dataGridView1.AutoGenerateColumns = false;
                bindingSource1.DataSource = await entities.movies
   .Select(
      movies =>
         new
         {
             Mov_id = movies.mov_id,
             Mov_title = movies.mov_title,
             Mov_year = movies.mov_year,
             Mov_time = movies.mov_time,
             Mov_lang = movies.mov_lang,
             Mov_dt_rel = movies.mov_dt_rel,
             Mov_rel_country = movies.mov_rel_country
         }
         )
   .ToListAsync();
                bindingNavigator1.BindingSource = bindingSource1;
                dataGridView1.DataSource = bindingSource1;
                dataGridView1.ClearSelection();
            }
        }


        private void btnAccept_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            int movieId = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column7"].Value);
            using (var entities = new FinalExamEntities())
            {
                var movie = entities.movies.FirstOrDefault(m => m.mov_id == movieId);
                this.Validate();
                if (movie != null)
                {
                    movie.mov_title = txtFilmName.Text;
                    movie.mov_year = Convert.ToInt32(txtFilmYear.Text);
                    movie.mov_time = Convert.ToInt32(txtFilmTime.Text);
                    movie.mov_lang = txtFilmLang.Text;
                    movie.mov_dt_rel = dateTimePicker1.Value;
                    movie.mov_rel_country = txtCountry.Text;
                    entities.SaveChanges();
                }
                refreshGridViewMovie();
            }
        }



        private async void toolStripButton1_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            using (var entities = new FinalExamEntities())
            {
                bindingSource1.DataSource = await entities.movies
  .Select(
     movies =>
        new
        {
            Mov_id = movies.mov_id,
            Mov_title = movies.mov_title,
            Mov_year = movies.mov_year,
            Mov_time = movies.mov_time,
            Mov_lang = movies.mov_lang,
            Mov_dt_rel = movies.mov_dt_rel,
            Mov_rel_country = movies.mov_rel_country
        }
        )
  .ToListAsync();
                clear();
                bindingNavigator1.BindingSource = bindingSource1;
                txtFilmName.DataBindings.Add("Text", bindingSource1, "mov_title", true, DataSourceUpdateMode.OnPropertyChanged);
                txtFilmYear.DataBindings.Add("Text", bindingSource1, "mov_year", true, DataSourceUpdateMode.OnPropertyChanged);
                txtFilmTime.DataBindings.Add("Text", bindingSource1, "mov_time", true, DataSourceUpdateMode.OnPropertyChanged);
                txtFilmLang.DataBindings.Add("Text", bindingSource1, "mov_lang", true, DataSourceUpdateMode.OnPropertyChanged);
                dateTimePicker1.DataBindings.Add("Text", bindingSource1, "mov_dt_rel", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCountry.DataBindings.Add("Text", bindingSource1, "mov_rel_country", true, DataSourceUpdateMode.OnPropertyChanged);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                var movieDelete = new movie
                {
                    mov_id = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column7"].Value)
                };
                entities.Entry(movieDelete).State = EntityState.Deleted;
                entities.SaveChanges();
                bindingSource1.RemoveCurrent();
                refreshGridViewMovie();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtFilmName.Clear();
            txtFilmYear.Clear();
            txtFilmTime.Clear();
            txtFilmLang.Clear();
            dateTimePicker1.Value = DateTime.Now;
            txtCountry.Clear();
        }

        public async void refreshGridViewMovie()
        {
            using (var entities = new FinalExamEntities())
            {
                dataGridView1.DataSource = await entities.movies
  .Select(
     movies =>
        new
        {
            Mov_id = movies.mov_id,
            Mov_title = movies.mov_title,
            Mov_year = movies.mov_year,
            Mov_time = movies.mov_time,
            Mov_lang = movies.mov_lang,
            Mov_dt_rel = movies.mov_dt_rel,
            Mov_rel_country = movies.mov_rel_country
        }
        )
  .ToListAsync();
            }
        }

        private void clear()
        {
            txtFilmName.DataBindings.Clear();
            txtFilmYear.DataBindings.Clear();
            txtFilmTime.DataBindings.Clear();
            txtFilmLang.DataBindings.Clear();
            dateTimePicker1.DataBindings.Clear();
            txtCountry.DataBindings.Clear();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            AddFilmForm addFilmForm = new AddFilmForm();
            addFilmForm.Show();
        }
    }
}


