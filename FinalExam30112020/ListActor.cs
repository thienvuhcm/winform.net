﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class ListActor : Form
    {
        public ListActor()
        {
            InitializeComponent();
        }

        private void ListActor_Load(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                dataGridView1.AutoGenerateColumns = false;
                bindingSource1.DataSource = entities.actors
   .Select(
      actors =>
         new
         {
             Act_id = actors.act_id,
             Act_fname = actors.act_fname,
             Act_lname = actors.act_lname,
             Act_gender = actors.act_gender
         }
   ).ToList();
                bindingNavigator1.BindingSource = bindingSource1;
                dataGridView1.DataSource = bindingSource1;
                dataGridView1.ClearSelection();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                var actorDelete = new actor
                {
                    act_id = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column3"].Value)
                };
                entities.Entry(actorDelete).State = EntityState.Deleted;
                entities.SaveChanges();
                bindingSource1.RemoveCurrent();
                refreshGridView();
            }
        }

        private void refreshGridView()
        {
            using (var entities = new FinalExamEntities())
            {
                bindingSource1.DataSource = entities.actors
   .Select(
      actors =>
         new
         {
             Act_id = actors.act_id,
             Act_fname = actors.act_fname,
             Act_lname = actors.act_lname,
             Act_gender = actors.act_gender
         }
   ).ToList();
            }
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            using (var entities = new FinalExamEntities())
            {
                clear();
                bindingNavigator1.BindingSource = bindingSource1;
                txtFullName.DataBindings.Add("Text", bindingSource1, "act_fname", true, DataSourceUpdateMode.OnPropertyChanged);
                txtLastName.DataBindings.Add("Text", bindingSource1, "act_lname", true, DataSourceUpdateMode.OnPropertyChanged);
                txtGender.DataBindings.Add("Text", bindingSource1, "act_gender", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }
        private void clear()
        {
            txtFullName.DataBindings.Clear();
            txtLastName.DataBindings.Clear();
            txtGender.DataBindings.Clear();
        }

        private void clearTextBox()
        {
            txtFullName.Clear();
            txtLastName.Clear();
            txtGender.Clear();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            dataGridView1.ClearSelection();
            clearTextBox();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            clearTextBox();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //int? actorId = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column3"].Value);
            using (var entities = new FinalExamEntities())
            {
                //var actor = entities.actors.FirstOrDefault(a => a.act_id == actorId);
                //this.Validate();
                //if (actor != null)
                //{
                //    clear();
                //    actor.act_fname = txtFullName.Text;
                //    actor.act_lname = txtLastName.Text;
                //    actor.act_gender = txtGender.Text;

                //}

                //if (actorId == null)
                //{
                actor newActor = new actor()
                {
                    act_fname = txtFullName.Text,
                    act_lname = txtLastName.Text,
                    act_gender = txtGender.Text
                };
                entities.actors.Add(newActor);
                entities.SaveChanges();
            }
            refreshGridView();
        }


        private async void bindingNavigatorDeleteItem_Click_1(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                var actorDelete = new actor
                {
                    act_id = Convert.ToInt32(dataGridView1.CurrentRow.Cells["Column3"].Value)
                };
                entities.Entry(actorDelete).State = EntityState.Deleted;
                await entities.SaveChangesAsync();
                bindingSource1.RemoveCurrent();
            }
            refreshGridView();
        }
    }
}
