﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalExam30112020
{
    public partial class AddFilmForm : Form
    {
        ListFilmForm listFilmForm = new ListFilmForm();
        public AddFilmForm()
        {
            InitializeComponent();
        }

        private async void AddFilmForm_Load(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {

                dataGridView1.AutoGenerateColumns = false;
                bindingSource1.DataSource = await entities.movies
   .Select(
      movies =>
         new
         {
             Mov_title = movies.mov_title,
             Mov_year = movies.mov_year,
             Mov_time = movies.mov_time,
             Mov_lang = movies.mov_lang,
             Mov_dt_rel = movies.mov_dt_rel,
             Mov_rel_country = movies.mov_rel_country
         }
         )
   .ToListAsync();
                bindingNavigator1.BindingSource = bindingSource1;
                dataGridView1.DataSource = bindingSource1;
                refreshGridView();
            }
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            using (var entities = new FinalExamEntities())
            {
                movie movie = new movie()
                {
                    mov_title = txtFilmName.Text,
                    mov_year = Convert.ToInt32(txtFilmYear.Text),
                    mov_time = Convert.ToInt32(txtFilmTime.Text),
                    mov_lang = txtFilmLang.Text,
                    mov_dt_rel = DateTime.Parse(dateTimePicker1.Value.ToShortDateString()),
                    mov_rel_country = txtCountry.Text,
                };
                entities.movies.Add(movie);
                entities.SaveChanges();
            }
            refreshGridView();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtFilmName.Clear();
            txtFilmYear.Clear();
            txtFilmTime.Clear();
            txtFilmLang.Clear();
            dateTimePicker1.Value = DateTime.Now;
            txtCountry.Clear();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            listFilmForm.refreshGridViewMovie();
            this.Close();
        }

        private async void refreshGridView()
        {
            using (var entities = new FinalExamEntities())
            {

                dataGridView1.AutoGenerateColumns = false;
                bindingSource1.DataSource = await entities.movies
   .Select(
      movies =>
         new
         {
             Mov_title = movies.mov_title,
             Mov_year = movies.mov_year,
             Mov_time = movies.mov_time,
             Mov_lang = movies.mov_lang,
             Mov_dt_rel = movies.mov_dt_rel,
             Mov_rel_country = movies.mov_rel_country
         }
         )
   .ToListAsync();
            }
            }
    }
}
