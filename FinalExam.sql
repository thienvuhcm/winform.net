USE [master]
GO
/****** Object:  Database [FinalExam]    Script Date: 27/11/2020 5:16:35 CH ******/
CREATE DATABASE [FinalExam]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FinalExam', FILENAME = N'G:\SQLServer2019\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\FinalExam.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FinalExam_log', FILENAME = N'G:\SQLServer2019\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\FinalExam.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [FinalExam] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FinalExam].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FinalExam] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [FinalExam] SET ANSI_NULLS ON 
GO
ALTER DATABASE [FinalExam] SET ANSI_PADDING ON 
GO
ALTER DATABASE [FinalExam] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [FinalExam] SET ARITHABORT ON 
GO
ALTER DATABASE [FinalExam] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FinalExam] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FinalExam] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FinalExam] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FinalExam] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [FinalExam] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [FinalExam] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FinalExam] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [FinalExam] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FinalExam] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FinalExam] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FinalExam] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FinalExam] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FinalExam] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FinalExam] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FinalExam] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FinalExam] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FinalExam] SET RECOVERY FULL 
GO
ALTER DATABASE [FinalExam] SET  MULTI_USER 
GO
ALTER DATABASE [FinalExam] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FinalExam] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FinalExam] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FinalExam] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FinalExam] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FinalExam] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [FinalExam] SET QUERY_STORE = OFF
GO
USE [FinalExam]
GO
/****** Object:  User [root]    Script Date: 27/11/2020 5:16:35 CH ******/
CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [root]
GO
/****** Object:  Table [dbo].[actor]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actor](
	[act_id] [int] IDENTITY(1,1) NOT NULL,
	[act_fname] [char](20) NULL,
	[act_lname] [char](20) NULL,
	[act_gender] [char](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[act_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[director]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[director](
	[dir_id] [int] IDENTITY(1,1) NOT NULL,
	[dir_fname] [char](20) NULL,
	[dir_lname] [char](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[dir_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[genres]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[genres](
	[gen_id] [int] IDENTITY(1,1) NOT NULL,
	[gen_title] [char](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[gen_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movie]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movie](
	[mov_id] [int] IDENTITY(1,1) NOT NULL,
	[mov_title] [char](50) NULL,
	[mov_year] [int] NULL,
	[mov_time] [int] NULL,
	[mov_lang] [char](50) NULL,
	[mov_dt_rel] [date] NULL,
	[mov_rel_country] [char](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[mov_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movie_cast]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movie_cast](
	[act_id] [int] NOT NULL,
	[mov_id] [int] NOT NULL,
	[role] [char](30) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movie_direction]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movie_direction](
	[dir_id] [int] NOT NULL,
	[mov_id] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[movie_genres]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movie_genres](
	[mov_id] [int] NOT NULL,
	[gen_id] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rating]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rating](
	[mov_id] [int] NOT NULL,
	[rev_id] [int] NOT NULL,
	[rev_starts] [int] NULL,
	[num_o_ratings] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reviewer]    Script Date: 27/11/2020 5:16:35 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reviewer](
	[rev_id] [int] IDENTITY(1,1) NOT NULL,
	[rev_name] [char](30) NULL,
	[rev_role] [char](30) NULL,
	[rev_username] [char](40) NULL,
	[rev_password] [char](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[rev_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movie_cast]  WITH CHECK ADD  CONSTRAINT [FK_movie_cast_actor] FOREIGN KEY([act_id])
REFERENCES [dbo].[actor] ([act_id])
GO
ALTER TABLE [dbo].[movie_cast] CHECK CONSTRAINT [FK_movie_cast_actor]
GO
ALTER TABLE [dbo].[movie_cast]  WITH CHECK ADD  CONSTRAINT [FK_movie_cast_movie] FOREIGN KEY([mov_id])
REFERENCES [dbo].[movie] ([mov_id])
GO
ALTER TABLE [dbo].[movie_cast] CHECK CONSTRAINT [FK_movie_cast_movie]
GO
ALTER TABLE [dbo].[movie_direction]  WITH CHECK ADD  CONSTRAINT [FK_movie_direction_director] FOREIGN KEY([dir_id])
REFERENCES [dbo].[director] ([dir_id])
GO
ALTER TABLE [dbo].[movie_direction] CHECK CONSTRAINT [FK_movie_direction_director]
GO
ALTER TABLE [dbo].[movie_direction]  WITH CHECK ADD  CONSTRAINT [FK_movie_direction_movie] FOREIGN KEY([mov_id])
REFERENCES [dbo].[movie] ([mov_id])
GO
ALTER TABLE [dbo].[movie_direction] CHECK CONSTRAINT [FK_movie_direction_movie]
GO
ALTER TABLE [dbo].[movie_genres]  WITH CHECK ADD  CONSTRAINT [FK_movie_genres_genres] FOREIGN KEY([gen_id])
REFERENCES [dbo].[genres] ([gen_id])
GO
ALTER TABLE [dbo].[movie_genres] CHECK CONSTRAINT [FK_movie_genres_genres]
GO
ALTER TABLE [dbo].[movie_genres]  WITH CHECK ADD  CONSTRAINT [FK_movie_genres_movie] FOREIGN KEY([mov_id])
REFERENCES [dbo].[movie] ([mov_id])
GO
ALTER TABLE [dbo].[movie_genres] CHECK CONSTRAINT [FK_movie_genres_movie]
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [FK_rating_movie] FOREIGN KEY([mov_id])
REFERENCES [dbo].[movie] ([mov_id])
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [FK_rating_movie]
GO
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [FK_rating_reviewer] FOREIGN KEY([rev_id])
REFERENCES [dbo].[reviewer] ([rev_id])
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [FK_rating_reviewer]
GO
USE [master]
GO
ALTER DATABASE [FinalExam] SET  READ_WRITE 
GO
